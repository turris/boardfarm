# Copyright (c) 2015
#
# All rights reserved.
#
# This file is distributed under the Clear BSD license.
# The full text can be found in LICENSE in the root directory.

import rootfs_boot
from devices import board, wan, lan, wlan, prompt

class Webserver_Running(rootfs_boot.RootFSBootTest):
    '''Router webserver is running.'''
    def runTest(self):
        board.sendline('\nps | grep -v grep | grep http')
        board.expect('uhttpd')
        board.expect(prompt)

class WebGUI_Access(rootfs_boot.RootFSBootTest):
    '''Router webpage available to LAN-device at http://192.168.1.1/.'''
    def runTest(self):
        ip = "192.168.1.1"
        url = 'http://%s/' % ip
        lan.sendline('\ncurl -v %s' % url)
        lan.expect('<html')
        lan.expect('<body')
        lan.expect('</body>')
        lan.expect('</html>')
        lan.expect(prompt)

class WebGUI_NoStackTrace(rootfs_boot.RootFSBootTest):
    '''Router webpage at cgi-bin/luci contains no stack traceback.'''
    def runTest(self):
        board.sendline('\ncurl -s http://127.0.0.1/cgi-bin/luci | head -15')
        board.expect('cgi-bin/luci')
        board.expect(prompt)
        assert 'traceback' not in board.before

class Webserver_Download(rootfs_boot.RootFSBootTest):
    '''Downloaded small file from router webserver in reasonable time.
    
    Requires:
    - the board
      - having a 192.168.1.1 IP address
    - the LAN device
      - having the curl 7.38 or compatible installed
      - having the iproute2 3.16 or compatible installed
      - having a manually configured eth1 interface connected to the board
    
    '''
    ADDRESS = '192.168.1.2/24'

    def setUp(self):
        super(Webserver_Download, self).setUp()

        lan.sendline('ip address add {0} dev eth1'.format(self.ADDRESS))
        lan.expect_prompt()

    def runTest(self):
        ip = "192.168.1.1"
        board.sendline('\nhead -c 1000000 /dev/urandom > /www/deleteme.txt')
        board.expect('head ', timeout=5)
        board.expect(prompt)
        lan.sendline('\ncurl -m 25 http://%s/deleteme.txt > /dev/null' % ip)
        lan.expect('Total', timeout=5)
        lan.expect('100 ', timeout=10)
        lan.expect(prompt, timeout=10)
        board.sendline('\nrm -f /www/deleteme.txt')
        board.expect('deleteme.txt')
        board.expect(prompt)
    def recover(self):
        board.sendcontrol('c')
        lan.sendcontrol('c')
        board.sendline('rm -f /www/deleteme.txt')
        board.expect(prompt)

    def tearDown(self):
        lan.sendline('ip address delete {0} dev eth1'.format(self.ADDRESS))
        lan.expect_prompt()

        super(Webserver_Download, self).tearDown()
