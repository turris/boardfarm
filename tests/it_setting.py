import devices.common
import linux_boot

class InternetSetting(linux_boot.LinuxBootTest):
	'''Sets the internet after fresh boot'''

	def runTest(self):
		'''Setting /etc/config/network'''
		devices.board.sendline('uci set network.wan.proto="dhcp"')
		devices.board.expect_prompt()
		devices.board.sendline('uci set network.wan6.ifname="@wan"')
		devices.board.expect_prompt()
		devices.board.sendline('uci set network.wan6.proto="dhcpv6"')
		devices.board.expect_prompt()
		devices.board.sendline('uci commit network')
		devices.board.expect_prompt()	
		devices.board.sendline('/etc/init.d/network restart')
		devices.board.expect_prompt()

		'''Setting time with ntpd.'''
		devices.board.sendline('/etc/init.d/sysntpd enable')
		devices.board.expect_prompt()
		devices.board.sendline('/etc/init.d/sysntpd start')

