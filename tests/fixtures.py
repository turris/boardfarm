# -*- coding: utf-8 -*-

import re
import time

import pexpect
import selenium.common.exceptions
import selenium.webdriver.support.expected_conditions

import devices.common
import lib
import linux_boot


class SetUpOmniaDeployFlashed(linux_boot.LinuxBootTest):

    """Flash the deploy branch of the latest Turris OS on the board.

    It disables DNS forwarding and installs the mtd package.

    Requires:
    - the board
      - being a Turris Omnia
    - the WAN device
      - having a 192.168.0.1 IP address

    """

    WAN_ADDRESS = '192.168.0.1'

    BOARD_ADDRESS = '192.168.0.2/24'

    def runTest(self):
        devices.common.sendline_robust(devices.board, 'ip address add {0} dev eth1'.format(self.BOARD_ADDRESS))
        devices.board.expect_prompt()
        devices.common.sendline_robust(devices.board, 'ip route add default via {0}'.format(self.WAN_ADDRESS))
        devices.board.expect_prompt()
        devices.common.sendline_robust(devices.board, 'uci set resolver.common.forward_upstream=0')
        devices.board.expect_prompt()
        devices.common.sendline_robust(devices.board, 'uci commit')
        devices.board.expect_prompt()
        devices.common.sendline_robust(devices.board, '/etc/init.d/resolver restart')
        devices.board.expect_prompt()
        devices.common.sendline_robust(devices.board, 'opkg update')
        devices.board.expect_prompt()
        devices.common.sendline_robust(devices.board, 'opkg install mtd')
        devices.board.expect_prompt()
        devices.common.sendline_robust(devices.board, 'mktemp -d')
        devices.board.expect_prompt()
        dirname = devices.board.before.strip()
        devices.common.sendline_robust(devices.board, 'wget --no-check-certificate --output-document={0}/mtd https://api.turris.cz/openwrt-repo/omnia/nor_fw/omnia-initramfs-zimage'.format(dirname))
        devices.board.expect_prompt()
        devices.common.sendline_robust(devices.board, 'wget --no-check-certificate --output-document={0}/uboot https://api.turris.cz/openwrt-repo/omnia/nor_fw/uboot-turris-omnia-spl.kwb'.format(dirname))
        devices.board.expect_prompt()
        devices.common.sendline_robust(devices.board, 'wget --no-check-certificate --output-document={0}/medkit.tar.gz https://api.turris.cz/openwrt-repo/omnia/medkit/omnia-medkit-latest.tar.gz'.format(dirname))
        devices.board.expect_prompt()
        devices.common.sendline_robust(devices.board, 'wget --no-check-certificate --output-document={0}/medkit.tar.gz https://api.turris.cz/openwrt-repo/omnia/medkit/omnia-medkit-latest.tar.gz'.format(dirname))
        devices.board.expect_prompt()
        devices.common.sendline_robust(devices.board, 'mtd write {0}/mtd /dev/mtd1'.format(dirname))
        devices.board.expect_prompt(timeout=90)
        devices.common.sendline_robust(devices.board, 'mtd write {0}/uboot /dev/mtd0'.format(dirname))
        devices.board.expect_prompt()
        devices.common.sendline_robust(devices.board, 'mkdir {0}/btrfs'.format(dirname))
        devices.board.expect_prompt()
        devices.common.sendline_robust(devices.board, 'mount /dev/mmcblk0p1 {0}/btrfs'.format(dirname))
        devices.board.expect_prompt()
        devices.common.sendline_robust(devices.board, 'btrfs subvolume delete {0}/btrfs/@factory'.format(dirname))
        devices.board.expect_prompt()
        devices.common.sendline_robust(devices.board, 'btrfs subvolume create {0}/btrfs/@factory'.format(dirname))
        devices.board.expect_prompt()
        devices.common.sendline_robust(devices.board, 'gunzip -c {0}/medkit.tar.gz | tar -x -C {0}/btrfs/@factory'.format(dirname))
        devices.board.expect_prompt()
        devices.common.sendline_robust(devices.board, 'btrfs subvolume delete {0}/btrfs/certbackup'.format(dirname))
        devices.board.expect_prompt()
        devices.common.sendline_robust(devices.board, 'umount {0}/btrfs'.format(dirname))
        devices.board.expect_prompt()
        devices.common.sendline_robust(devices.board, 'rm -rf {0}'.format(dirname))
        devices.board.expect_prompt()

    def recover(self):
        pass


class SetUpStateReset(linux_boot.LinuxBootTest):

    """Reset the board to the initial state."""

    def runTest(self):
        devices.board.reset_initial()

    def recover(self):
        pass


class SetUpTurrisBranchSet(linux_boot.LinuxBootTest):

    """Set the branch of the Turris OS.

    Requires:
    - the board
      - containing Turris OS 3.8 or higher
    - a BRANCH environment variable set to the name of the desired branch

    """

    def runTest(self):
        devices.board.set_branch()

    def recover(self):
        pass


class SetUpTurrisWizardFinished(linux_boot.LinuxBootTest):

    """Finish the Foris Wizard.

    It configures the board to have a 192.168.1.1 IP address and to obtain an
    address for the WAN interface via DHCP.

    Requires:
    - the board
      - reset to the initial state of Turris OS 1.0 if it is a Turris
      - reset to the initial state of Turris OS 3.2 or 3.8.x if it is a Turris Omnia
    - the LAN device
      - having the GNU Core Utilities 8.23 or compatible installed
      - having the ISC DHCP client 4.3 or compatible installed
      - having a manually configured eth1 interface connected to the board
      - running a Selenium Server 3.4 or compatible listening on the port 4444
      - having Mozilla Firefox 45.8 or compatible installed
    - a NTP server in the WAN
    - a HTTPS server in the WAN
      - serving the necessary Updater repositories
      - serving the necessary OpenWrt repositories
      - serving the "resources/ca/crl" at a "/crl" path
    - 13 DNS servers in the WAN
      - serving a SOA record for the root zone
      - serving following NS records:
        - . a.root-servers.net.
        - . b.root-servers.net.
        - . c.root-servers.net.
        - . d.root-servers.net.
        - . e.root-servers.net.
        - . f.root-servers.net.
        - . g.root-servers.net.
        - . h.root-servers.net.
        - . i.root-servers.net.
        - . j.root-servers.net.
        - . k.root-servers.net.
        - . l.root-servers.net.
        - . m.root-servers.net.
      - mapping:
        - "ntp.nic.cz" to an IP address of the NTP server
        - "www.rhybar.cz" to an IP address
        - "api.turris.cz" to an IP address of the HTTPS server
        - "a.root-servers.net" to an IP address of the 1st server
        - "b.root-servers.net" to an IP address of the 2nd server
        - "c.root-servers.net" to an IP address of the 3rd server
        - "d.root-servers.net" to an IP address of the 4th server
        - "e.root-servers.net" to an IP address of the 5th server
        - "f.root-servers.net" to an IP address of the 6th server
        - "g.root-servers.net" to an IP address of the 7th server
        - "h.root-servers.net" to an IP address of the 8th server
        - "i.root-servers.net" to an IP address of the 9th server
        - "j.root-servers.net" to an IP address of the 10th server
        - "k.root-servers.net" to an IP address of the 11th server
        - "l.root-servers.net" to an IP address of the 12th server
        - "m.root-servers.net" to an IP address of the 13th server
      - returning all DNS records signed except the "www.rhybar.cz" record
    - a DHCP server in the WAN
      - providing also an IP address of one of the DNS servers
    - a machine with a 217.31.205.50 IP address in the WAN

    """

    PASSWORD = 'bigfoot1'

    LAN_TEMPORARY_DN = '/var/lib/boardfarm'

    DHCLIENT_LEASES = 'dhclient.leases'

    DHCLIENT_PID = 'dhclient.pid'

    def setUp(self):
        super(SetUpTurrisWizardFinished, self).setUp()

        self.browser = _ExtendedBrowser(self._instantiate_browser())

        devices.lan.sendline('mkdir {0}'.format(self.LAN_TEMPORARY_DN))
        devices.lan.expect_prompt()
        devices.lan.sendline('dhclient -cf /dev/null -lf "{0}/{1}" -pf "{0}/{2}" eth1'.format(self.LAN_TEMPORARY_DN, self.DHCLIENT_LEASES, self.DHCLIENT_PID))
        devices.lan.expect_prompt()

    def runTest(self):
        self.browser.reduced.get('http://192.168.1.1/')

        self.browser.wait_any_text_displayed({'Welcome to configuration of router Turris', 'Vítejte v nastavení routeru Turris'})
        self.browser.wait_document_loaded()
        if self.browser.find_displayed_elements_by_text('Vítejte v nastavení routeru Turris'):
            print('vitejte v nastaveni routeru turris displayed')
            englishs = self.browser.find_displayed_elements_by_text('ENG')
            englishs[0].click()
            self.browser.wait_element_stale(englishs[0])

        self.browser.wait_any_text_displayed({'Welcome to configuration of router Turris'})
        self.browser.wait_document_loaded()
        begins = self.browser.find_displayed_elements_by_text('Begin installation')
        begins[0].click()
        self.browser.wait_element_stale(begins[0])

        self.browser.wait_any_text_displayed({'Password'})
        self.browser.wait_document_loaded()
        password_labels = self.browser.find_displayed_elements_by_text('Password', 'label')
        password_id = password_labels[0].get_attribute('for')
        password = self.browser.reduced.find_element_by_id(password_id)
        password.send_keys(self.PASSWORD)
        password2_labels = self.browser.find_displayed_elements_by_text('Password (repeat)', 'label')
        password2_id = password2_labels[0].get_attribute('for')
        password2 = self.browser.reduced.find_element_by_id(password2_id)
        password2.send_keys(self.PASSWORD)
        nexts = self.browser.find_displayed_elements_by_text('Next')
        nexts[0].click()
        self.browser.wait_element_stale(nexts[0])

        self.browser.wait_any_text_displayed({'WAN'})
        self.browser.wait_document_loaded()
        nexts = self.browser.find_displayed_elements_by_text('Next')
        nexts[0].click()
        self.browser.wait_element_stale(nexts[0])

        self.browser.wait_any_text_displayed({'Connectivity test'})
        self.browser.wait_document_loaded()
        for _ in range(2):
            start = time.time()
            try:
                self.browser.wait_any_text_displayed({'Internet connectivity test passed without error.'}, 60)
            except selenium.common.exceptions.TimeoutException:
                print('Internet connectivity test stuck')
                self.browser.reduced.refresh()
                continue
            print('Internet connectivity test took {} seconds'.format(time.time() - start))
            break
        else:
            raise ValueError('Internet connectivity test failed')
        nexts = self.browser.find_displayed_elements_by_text('Next')
        nexts[0].click()
        self.browser.wait_element_stale(nexts[0])

        self.browser.wait_any_text_displayed({'Region settings', 'Time settings'})
        self.browser.wait_document_loaded()
        if self.browser.find_displayed_elements_by_text('Region settings'):
            print('region settings displayed')
            nexts = self.browser.find_displayed_elements_by_text('Next')
            nexts[0].click()
            self.browser.wait_element_stale(nexts[0])

        self.browser.wait_any_text_displayed({'Time settings'})
        self.browser.wait_document_loaded()
        start = time.time()
        self.browser.wait_any_text_displayed({'Time was successfully synchronized, you can move to the next step.'}, 60)
        print('time synchronization took {} seconds'.format(time.time() - start))
        nexts = self.browser.find_displayed_elements_by_text('Next')
        nexts[0].click()
        self.browser.wait_element_stale(nexts[0])

        devices.common.sendline_robust(devices.board, 'cat /etc/turris-version')
        found = devices.board.expect(['cat: can\'t open \'/etc/turris-version\': No such file or directory', r'3\.8', r'\d\..*'], timeout=3)
        if found == 0:
            self.browser.wait_any_text_displayed({'System update'})
            self.browser.wait_document_loaded()
            start = time.time()
            while time.time() - start < 10 * 60:
                if self.browser.find_displayed_elements_by_text('Firmware update was successful, you can proceed to the next step.'):
                    break
                self._reconfigure()
                time.sleep(0.5)
            else:
                raise ValueError('Update failed')
            print('1st system update took {} seconds'.format(time.time() - start))
            nexts = self.browser.find_displayed_elements_by_text('Next')
            nexts[0].click()
            self.browser.wait_element_stale(nexts[0])

            self.browser.wait_any_text_displayed({'System update'})
            self.browser.wait_document_loaded()
            start = time.time()
            refresh_check = False
            while time.time() - start < 30 * 60:
                eula_labels = self.browser.find_displayed_elements_by_text('Use automatic updates (recommended)', 'label')
                if eula_labels:
                    print('EULA displayed')
                    eula_id = eula_labels[0].get_attribute('for')
                    eula = self.browser.reduced.find_element_by_id(eula_id)
                    eula.submit()
                if self.browser.find_displayed_elements_by_text('Firmware update was successful, you can proceed to the next step.'):
                    break
                # Workaround: https://lists.nic.cz/cgi-bin/mailman/private/router/2017-April/010334.html
                for text in {'Firmware update has failed due to a connection or installation error.', 'Unable to connect', 'The connection was reset'}:
                    if self.browser.find_displayed_elements_by_text(text):
                        print('{} displayed'.format(text))
                        self.browser.reduced.refresh()
                        refresh_check = True
                # Workaround: https://lists.nic.cz/cgi-bin/mailman/private/router/2017-May/010452.html
                if refresh_check and self.browser.find_displayed_elements_by_text('Check of available updates in progress.'):
                    self.browser.reduced.refresh()
                    refresh_check = False
                if self.browser.find_displayed_elements_by_text('You have been logged out due to longer inactivity.'):
                    print('login due to inactivity displayed')
                    passwords = self.browser.find_displayed_elements_by_tag('input')
                    passwords[0].send_keys(self.PASSWORD)
                    logins = self.browser.find_displayed_elements_by_text('Log in')
                    logins[0].click()
                if self.browser.find_displayed_elements_by_text('Please log in again to continue.'):
                    print('login again displayed')
                    proceeds = self.browser.find_displayed_elements_by_text('Proceed to login')
                    proceeds[0].click()
                    passwords = self.browser.find_displayed_elements_by_tag('input')
                    passwords[0].send_keys(self.PASSWORD)
                    logins = self.browser.find_displayed_elements_by_text('Log in')
                    logins[0].click()
                found = devices.board.expect(['\r\nRestarting system.', '\r\nU-Boot ', '\r\nHit any key ', pexpect.TIMEOUT], timeout=0)
                if found < 3:
                    devices.board.expect('Please press Enter to activate this console', timeout=30)
                    devices.board.sendline()
                    devices.board.expect_prompt()
                self._reconfigure()
                time.sleep(0.5)
            else:
                raise ValueError('Update failed')
            print('2nd system update took {} seconds'.format(time.time() - start))
            nexts = self.browser.find_displayed_elements_by_text('Next')
            nexts[0].click()
            self.browser.wait_element_stale(nexts[0])

            self.browser.wait_any_text_displayed({'Automatic updates', 'LAN'})
            self.browser.wait_document_loaded()
            if self.browser.find_displayed_elements_by_text('Automatic updates'):
                print('3rd automatic updates displayed')
                start = time.time()
                while time.time() - start < 15 * 60:
                    if self.browser.find_displayed_elements_by_text('Firmware update was successful, you can proceed to the next step.'):
                        break
                    self._reconfigure()
                    time.sleep(0.5)
                else:
                    raise ValueError('Update failed')
                print('3rd system update took {} seconds'.format(time.time() - start))
                nexts = self.browser.find_displayed_elements_by_text('Next')
                nexts[0].click()
                self.browser.wait_element_stale(nexts[0])
        elif found == 1:
            self.browser.wait_any_text_displayed({'Automatic updates'})
            self.browser.wait_document_loaded()
            nexts = self.browser.find_displayed_elements_by_text('Next')
            nexts[0].click()
            self.browser.wait_element_displayed(nexts[0], displayed=False)

            self.browser.wait_any_text_displayed({'Automatic updates'})
            self.browser.wait_document_loaded()
            start = time.time()
            while time.time() - start < 5 * 60:
                if self.browser.find_displayed_elements_by_text('Firmware update was successful, you can proceed to the next step.'):
                    break
                self._reconfigure()
                time.sleep(0.5)
            else:
                raise ValueError('Update failed')
            print('system update took {} seconds'.format(time.time() - start))
            nexts = self.browser.find_displayed_elements_by_text('Next')
            nexts[0].click()
            self.browser.wait_element_stale(nexts[0])
        else:
            self.browser.wait_any_text_displayed({'Automatic updates'})
            self.browser.wait_document_loaded()
            nexts = self.browser.find_displayed_elements_by_text('Next')
            nexts[0].click()
            self.browser.wait_element_displayed(nexts[0], displayed=False)

            self.browser.wait_any_text_displayed({'Automatic updates'})
            self.browser.wait_document_loaded()
            start = time.time()
            while time.time() - start < 5 * 60:
                # Workaround: https://lists.nic.cz/cgi-bin/mailman/private/router/2017-May/010492.html
                if self.browser.find_displayed_elements_by_text('Firmware update has failed due to a connection or installation error.'):
                    break
                self._reconfigure()
                time.sleep(0.5)
            else:
                raise ValueError('Update failed')
            print('1st system update took {} seconds'.format(time.time() - start))
            devices.board.reset()
            start = time.time()
            devices.board.expect('Router Turris successfully started.', timeout=600)
            print('1st reboot took {} seconds'.format(time.time() - start))
            devices.board.sendline()
            devices.board.expect_prompt()
            self.browser.reduced.refresh()

            self.browser.wait_any_text_displayed({'You have been logged out due to longer inactivity.'})
            self.browser.wait_document_loaded()
            passwords = self.browser.find_displayed_elements_by_tag('input')
            passwords[0].send_keys(self.PASSWORD)
            logins = self.browser.find_displayed_elements_by_text('Log in')
            logins[0].click()
            self.browser.wait_element_stale(logins[0])

            self.browser.wait_any_text_displayed({'Automatic updates'})
            self.browser.wait_document_loaded()
            nexts = self.browser.find_displayed_elements_by_text('Next')
            nexts[0].click()
            self.browser.wait_element_displayed(nexts[0], displayed=False)

            self.browser.wait_any_text_displayed({'Automatic updates'})
            self.browser.wait_document_loaded()
            start = time.time()
            while time.time() - start < 1 * 60:
                if self.browser.find_displayed_elements_by_text('Firmware update was successful, you can proceed to the next step.'):
                    break
                self._reconfigure()
                time.sleep(0.5)
            else:
                raise ValueError('Update failed')
            print('2nd system update took {} seconds'.format(time.time() - start))
            nexts = self.browser.find_displayed_elements_by_text('Next')
            nexts[0].click()
            self.browser.wait_element_stale(nexts[0])

        self.browser.wait_any_text_displayed({'LAN'})
        self.browser.wait_document_loaded()
        nexts = self.browser.find_displayed_elements_by_text('Next')
        nexts[0].click()
        self.browser.wait_element_stale(nexts[0])

        self.browser.wait_any_text_displayed({'Wi-Fi'})
        self.browser.wait_document_loaded()
        enable1_labels = self.browser.find_displayed_elements_by_text('Enable Wi-Fi 1', 'label')
        enable1_id = enable1_labels[0].get_attribute('for')
        enable1 = self.browser.reduced.find_element_by_id(enable1_id)
        enable1.click()
        wait = selenium.webdriver.support.wait.WebDriverWait(self.browser, 5)
        wait.until(lambda browser: len(browser.find_displayed_elements_by_text('Network password')) == 1)
        enable2_labels = self.browser.find_displayed_elements_by_text('Enable Wi-Fi 2', 'label')
        enable2_id = enable2_labels[0].get_attribute('for')
        enable2 = self.browser.reduced.find_element_by_id(enable2_id)
        enable2.click()
        wait = selenium.webdriver.support.wait.WebDriverWait(self.browser, 5)
        wait.until(lambda browser: len(browser.find_displayed_elements_by_text('Network password')) == 0)
        nexts = self.browser.find_displayed_elements_by_text('Next')
        nexts[0].click()
        self.browser.wait_element_stale(nexts[0])

        if self.browser.find_displayed_elements_by_text('Important updates that require a device restart have been installed.'):
            print('restart required')
            self.browser.wait_any_text_displayed({'Installation finished'})
            self.browser.wait_document_loaded()
            devices.board.reset()
            start = time.time()
            devices.board.expect('Router Turris successfully started.', timeout=300)
            print('2nd reboot took {} seconds'.format(time.time() - start))
            devices.board.sendline()
            devices.board.expect_prompt()

    def recover(self):
        pass

    def tearDown(self):
        self.browser.reduced.close()

        devices.lan.sendline('dhclient -lf "{0}/{1}" -pf "{0}/{2}" -r'.format(self.LAN_TEMPORARY_DN, self.DHCLIENT_LEASES, self.DHCLIENT_PID))
        devices.lan.expect_prompt()
        devices.lan.sendline('rm --recursive --force {0}'.format(self.LAN_TEMPORARY_DN))
        devices.lan.expect_prompt()

        super(SetUpTurrisWizardFinished, self).tearDown()

    def _instantiate_browser(self):
        return lib.common.phantom_webproxy_driver('http://' + devices.lan.name + ':8080')

    def _reconfigure(self):
        pass


class SetUpTurrisWizardFinishedUsingCustomKeys(SetUpTurrisWizardFinished):

    """Finish the Foris Wizard using custom keys.

    It configures the board to have a 192.168.1.1 IP address and to obtain an
    address for the WAN interface via DHCP.

    Requires:
    - the board
      - reset to the initial state of Turris OS 1.0 if it is a Turris
      - reset to the initial state of Turris OS 3.2 or 3.8.x if it is a Turris Omnia
    - the LAN device
      - having the GNU Core Utilities 8.23 or compatible installed
      - having the ISC DHCP client 4.3 or compatible installed
      - having a manually configured eth1 interface connected to the board
      - running a Selenium Server 3.4 or compatible listening on the port 4444
      - having Mozilla Firefox 45.8 or compatible installed
    - a NTP server in the WAN
    - a HTTPS server in the WAN
      - using a certificate signed by "resources/ca/cert.pem"
      - serving the necessary Updater repositories
      - serving the necessary OpenWrt repositories signed using
        "resources/usign/release.key"
      - serving the "resources/ca/crl" at a "/crl" path
    - 13 DNS servers in the WAN
      - serving a SOA record for the root zone
      - serving following NS records:
        - . a.root-servers.net.
        - . b.root-servers.net.
        - . c.root-servers.net.
        - . d.root-servers.net.
        - . e.root-servers.net.
        - . f.root-servers.net.
        - . g.root-servers.net.
        - . h.root-servers.net.
        - . i.root-servers.net.
        - . j.root-servers.net.
        - . k.root-servers.net.
        - . l.root-servers.net.
        - . m.root-servers.net.
      - mapping:
        - "ntp.nic.cz" to an IP address of the NTP server
        - "www.rhybar.cz" to an IP address
        - "api.turris.cz" to an IP address of the HTTPS server
        - "a.root-servers.net" to an IP address of the 1st server
        - "b.root-servers.net" to an IP address of the 2nd server
        - "c.root-servers.net" to an IP address of the 3rd server
        - "d.root-servers.net" to an IP address of the 4th server
        - "e.root-servers.net" to an IP address of the 5th server
        - "f.root-servers.net" to an IP address of the 6th server
        - "g.root-servers.net" to an IP address of the 7th server
        - "h.root-servers.net" to an IP address of the 8th server
        - "i.root-servers.net" to an IP address of the 9th server
        - "j.root-servers.net" to an IP address of the 10th server
        - "k.root-servers.net" to an IP address of the 11th server
        - "l.root-servers.net" to an IP address of the 12th server
        - "m.root-servers.net" to an IP address of the 13th server
      - returning all DNS records signed using the "resources/dns/zsk.private"
        and "resources/dns/ksk.private" except the "www.rhybar.cz" record
    - a DHCP server in the WAN
      - providing also an IP address of one of the DNS servers
    - a machine with a 217.31.205.50 IP address in the WAN

    """

    BOARD_TEMPORARY_DN = '/usr/share/boardfarm'

    ROOT_KEY = 'root.key'

    CA_CERT = 'updater.pem'

    USIGN_KEY = 'release.pub'

    RECONFIGURE = 'reconfigure.sh'

    RECONFIGURE_CONTENT = (
        'if [ -f /etc/init.d/resolver ]; then\n'
        '  ROOT_KEY=/etc/root.keys\n'
        '  RESOLVER=/etc/init.d/resolver\n'
        'else\n'
        '  ROOT_KEY=/etc/unbound/root.key\n'
        '  RESOLVER=/etc/init.d/unbound\n'
        'fi\n'
        'if ! grep -qF "{ksk}" $ROOT_KEY; then\n'
        # We need to replace $ROOT_KEY atomically, thus mv instead of cp.
        # And we need to call this multiple times, thus cp before mv.
        '  cp {dir}/{root} {dir}/root.key.tmp\n'
        '  mv {dir}/root.key.tmp $ROOT_KEY\n'
        '  $RESOLVER restart\n'
        'fi\n'
        # We need to replace updater.pem atomically, thus mv instead of cp.
        # And we need to call this multiple times, thus cp before mv.
        'cp {dir}/{ca} {dir}/updater.pem.tmp\n'
        'mv {dir}/updater.pem.tmp /etc/ssl/updater.pem\n'
        # We need to replace release.pub atomically, thus mv instead of cp.
        # And we need to call this multiple times, thus cp before mv.
        'if [ -f /etc/updater/keys/release.pub ]; then\n'
        '  cp {dir}/{usign} {dir}/release.pub.tmp\n'
        '  mv {dir}/release.pub.tmp /etc/updater/keys/release.pub\n'
        'fi'
        .format(ca=CA_CERT, dir='{dir}', ksk='{ksk}', root=ROOT_KEY, usign=USIGN_KEY))

    def setUp(self):
        super(SetUpTurrisWizardFinishedUsingCustomKeys, self).setUp()

        with open('resources/dns/ksk.key') as ksk_file:
            match = re.search(r'^\.\sIN\sDNSKEY\s257\s3\s\d+\s([^;]+)(?:\s+;.*)?$', ksk_file.read(), re.MULTILINE)
            ksk_record = match.group(0)
            ksk = match.group(1)
        with open('resources/ca/cert.pem') as ca_file:
            ca_cert = ca_file.read()
        with open('resources/usign/release.pub') as usign_file:
            usign_key = usign_file.read()

        devices.common.sendline_robust(devices.board, 'mkdir {0}'.format(self.BOARD_TEMPORARY_DN))
        devices.board.expect_prompt()
        for command in devices.common.compose_write_file(self.BOARD_TEMPORARY_DN + '/' + self.ROOT_KEY, ksk_record.replace('\t', ' ')):
            devices.common.sendline_robust(devices.board, command)
            devices.board.expect_prompt()
        for command in devices.common.compose_write_file(self.BOARD_TEMPORARY_DN + '/' + self.CA_CERT, ca_cert):
            devices.common.sendline_robust(devices.board, command)
            devices.board.expect_prompt()
        for command in devices.common.compose_write_file(self.BOARD_TEMPORARY_DN + '/' + self.USIGN_KEY, usign_key):
            devices.common.sendline_robust(devices.board, command)
            devices.board.expect_prompt()
        for command in devices.common.compose_write_file(self.BOARD_TEMPORARY_DN + '/' + self.RECONFIGURE, self.RECONFIGURE_CONTENT.format(dir=self.BOARD_TEMPORARY_DN, ksk=ksk)):
            devices.common.sendline_robust(devices.board, command)
            devices.board.expect_prompt()

        # Set testing keys.
        self._reconfigure()

    def tearDown(self):
        devices.common.sendline_robust(devices.board, 'rm -rf {0}'.format(self.BOARD_TEMPORARY_DN))
        devices.board.expect_prompt()

        super(SetUpTurrisWizardFinishedUsingCustomKeys, self).tearDown()

    def _instantiate_browser(self):
        return selenium.webdriver.Remote(
            'http://{}:4444/wd/hub'.format(devices.lan.name),
            {'browserName': 'firefox', 'marionette': False})

    def _reconfigure(self):
        devices.board.sendline('sh {0}/{1}'.format(self.BOARD_TEMPORARY_DN, self.RECONFIGURE))
        devices.board.expect(devices.board.prompt + [pexpect.TIMEOUT], timeout=3)


class _ExtendedBrowser(object):

    """An extended interface to a browser."""

    def __init__(self, reduced):
        super(_ExtendedBrowser, self).__init__()
        self.reduced = reduced

    def find_displayed_elements_by_tag(self, tag):
        for _ in range(10):
            elements = self.reduced.find_elements_by_tag_name(tag)
            try:
                return [element for element in elements if element.is_displayed()]
            except selenium.common.exceptions.StaleElementReferenceException:
                continue
        else:
            raise ValueError('elements too stale')

    def find_displayed_elements_by_text(self, text, tag='*'):
        """Find displayed elements by a text.

        The text must not contain a quote character (").

        """
        xpath = '//{0}[text()[contains(.,"{1}")]]'.format(tag, text)
        for _ in range(10):
            elements = self.reduced.find_elements_by_xpath(xpath)
            try:
                return [element for element in elements if element.is_displayed()]
            except selenium.common.exceptions.StaleElementReferenceException:
                continue
        else:
            raise ValueError('elements too stale')

    def wait_any_text_displayed(self, texts, timeout=3):
        wait = selenium.webdriver.support.wait.WebDriverWait(self, timeout)
        wait.until(lambda browser: any(bool(browser.find_displayed_elements_by_text(text)) for text in texts))

    def wait_element_displayed(self, element, timeout=3, displayed=True):
        wait = selenium.webdriver.support.wait.WebDriverWait(self.reduced, timeout)
        wait_until = wait.until if displayed else wait.until_not
        wait_until(selenium.webdriver.support.expected_conditions.visibility_of(element))

    def wait_element_stale(self, element, timeout=3):
        wait = selenium.webdriver.support.wait.WebDriverWait(self.reduced, timeout)
        wait.until(selenium.webdriver.support.expected_conditions.staleness_of(element))

    def wait_document_loaded(self, timeout=3):
        wait = selenium.webdriver.support.wait.WebDriverWait(self.reduced, timeout)
        wait.until(lambda browser: browser.execute_script('return document.readyState;') == 'complete')
