# Copyright (c) 2015
#
# All rights reserved.
#
# This file is distributed under the Clear BSD license.
# The full text can be found in LICENSE in the root directory.

import rootfs_boot
import lib
from devices import board, wan, lan, wlan, prompt

class RouterPingWanDev(rootfs_boot.RootFSBootTest):
    '''Router can ping device through WAN interface.

    Requires:
    - the board
      - having the BusyBox 1.25 or compatible installed
      - having a route to the WAN
    - a machine with a 192.168.0.1 IP address in the WAN

    '''
    def runTest(self):
        if not wan:
            msg = 'No WAN Device defined, skipping ping WAN test.'
            lib.common.test_msg(msg)
            self.skipTest(msg)
        board.sendline('\nping -c5 %s' % wan.gw)
        board.expect('5 (packets )?received', timeout=15)
        board.expect(prompt)
    def recover(self):
        board.sendcontrol('c')

class RouterPingInternet(rootfs_boot.RootFSBootTest):
    '''Router can ping internet address by IP.'''
    def runTest(self):
        board.sendline('\nping -c2 8.8.8.8')
        board.expect('2 (packets )?received', timeout=15)
        board.expect(prompt)

class RouterPingInternetName(rootfs_boot.RootFSBootTest):
    '''Router can ping internet address by name.'''
    def runTest(self):
        board.sendline('\nping -c2 www.google.com')
        board.expect('2 (packets )?received', timeout=15)
        board.expect(prompt)

class LanDevPingRouter(rootfs_boot.RootFSBootTest):
    '''Device on LAN can ping router.

    Requires:
    - the board
      - having a 192.168.1.1 IP address
    - the LAN device
      - having the iproute2 3.16 or compatible installed
      - having the ping command from the iputils 20121221 or compatible
        installed
      - having a manually configured eth1 interface connected to the board

    '''
    ADDRESS = '192.168.1.2/24'

    def setUp(self):
        super(LanDevPingRouter, self).setUp()

        board.sendline('ip address add {0} dev eth1'.format(self.ADDRESS))
        board.expect_prompt()

    def runTest(self):
        if not lan:
            msg = 'No LAN Device defined, skipping ping test from LAN.'
            lib.common.test_msg(msg)
            self.skipTest(msg)
        router_ip = board.get_interface_ipaddr(board.lan_iface)
        print(router_ip)
        lan.sendline('\nping -i 0.2 -c 5 %s' % router_ip)
        lan.expect('PING ')
        lan.expect('5 (packets )?received', timeout=15)
        lan.expect(prompt)

    def tearDown(self):
        board.sendline('ip address delete {0} dev eth1'.format(self.ADDRESS))
        board.expect_prompt()

        super(LanDevPingRouter, self).tearDown()

class LanDevPingWanDev(rootfs_boot.RootFSBootTest):
    '''Device on LAN can ping through router.

    Requires:
    - the board
      - having a 192.168.1.1 IP address
      - having a route to the WAN
    - the LAN device
      - having the iproute2 3.16 or compatible installed
      - having the ping command from the iputils 20121221 or compatible
        installed
      - having a manually configured eth1 interface connected to the board
    - a machine with a 192.168.0.1 IP address in the WAN
    
    '''
    BOARD_ADDRESS = '192.168.1.1'

    LAN_ADDRESS = '192.168.1.2/24'

    def setUp(self):
        super(LanDevPingWanDev, self).setUp()

        lan.sendline('ip address add {0} dev eth1'.format(self.LAN_ADDRESS))
        lan.expect_prompt()
        lan.sendline('ip route add default via {0}'.format(self.BOARD_ADDRESS))
        lan.expect_prompt()

    def runTest(self):
        if not lan:
            msg = 'No LAN Device defined, skipping ping test from LAN.'
            lib.common.test_msg(msg)
            self.skipTest(msg)
        if not wan:
            msg = 'No WAN Device defined, skipping ping WAN test.'
            lib.common.test_msg(msg)
            self.skipTest(msg)
        lan.sendline('\nping -i 0.2 -c 5 %s' % wan.gw)
        lan.expect('PING ')
        lan.expect('5 (packets )?received', timeout=15)
        lan.expect(prompt)
    def recover(self):
        lan.sendcontrol('c')

    def tearDown(self):
        lan.sendline('ip route delete default via {0}'.format(self.BOARD_ADDRESS))
        lan.expect_prompt()
        lan.sendline('ip address delete {0} dev eth1'.format(self.LAN_ADDRESS))
        lan.expect_prompt()

        super(LanDevPingWanDev, self).tearDown()

class LanDevPingInternet(rootfs_boot.RootFSBootTest):
    '''Device on LAN can ping through router to internet.'''
    def runTest(self):
        if not lan:
            msg = 'No LAN Device defined, skipping ping test from LAN.'
            lib.common.test_msg(msg)
            self.skipTest(msg)
        lan.sendline('\nping -c2 8.8.8.8')
        lan.expect('2 (packets )?received', timeout=10)
        lan.expect(prompt)
    def recover(self):
        lan.sendcontrol('c')
