import devices
import linux_boot


class PerformsRoutingTest(linux_boot.LinuxBootTest):

    """Test that the board performs routing.

    Requires:
    - the board
      - having a 192.168.1.1 IP address
      - having a route to the WAN
    - the LAN device
      - having the iproute2 3.16 or compatible installed
      - having the ping command from the iputils 20121221 or compatible
        installed
      - having a manually configured eth1 interface connected to the board
    - a machine with a 217.31.205.50 IP address in the WAN

    """

    BOARD_ADDRESS = '192.168.1.1'

    LAN_ADDRESS = '192.168.1.2/24'

    def setUp(self):
        super(PerformsRoutingTest, self).setUp()

        devices.lan.sendline('ip address add {0} dev eth1'.format(self.LAN_ADDRESS))
        devices.lan.expect_prompt()
        devices.lan.sendline('ip route add default via {0}'.format(self.BOARD_ADDRESS))
        devices.lan.expect_prompt()

    def runTest(self):
        devices.lan.sendline('ping -c 3 217.31.205.50')
        devices.lan.expect_prompt()
        devices.lan.sendline('echo $?')
        devices.lan.expect('0', timeout=1)

    def tearDown(self):
        devices.lan.sendline('ip route delete default via {0}'.format(self.BOARD_ADDRESS))
        devices.lan.expect_prompt()
        devices.lan.sendline('ip address delete {0} dev eth1'.format(self.LAN_ADDRESS))
        devices.lan.expect_prompt()

        super(PerformsRoutingTest, self).tearDown()
