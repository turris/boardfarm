import devices.common
import linux_boot
import os

class RamBoot(linux_boot.LinuxBootTest):
    '''
    Booting Omnia into RAM
    
    Requires:
    - repo.turris.cz
    - tftp server on 192.168.0.1
        -tftp-hpa, tar, cpio, u-boot-tools, wget packages        
    - a BRANCH environment variable set
    '''
    def runTest(self):

        #Preparing image on tftp server
        branch = os.getenv('BRANCH', 'stable')
        devices.wan.sendline('rm -r /srv/tftp/{}'.format(branch))
        devices.wan.expect_prompt()
        devices.wan.sendline('mkdir /srv/tftp/{}'.format(branch))
        devices.wan.expect_prompt()
        devices.wan.sendline('cd /srv/tftp/{}'.format(branch))
        devices.wan.expect_prompt()
        devices.wan.sendline('wget https://repo.turris.cz/omnia-{}/medkit/omnia-medkit-latest.tar.gz'.format(branch))
        devices.wan.expect_prompt()
        devices.wan.sendline('mkdir root')
        devices.wan.expect_prompt()
        devices.wan.sendline('tar -xzf omnia-medkit-latest.tar.gz -C root')
        devices.wan.expect_prompt(timeout=15)
        devices.wan.sendline('cp root/boot/zImage-* image')
        devices.wan.expect_prompt()
        devices.wan.sendline('cd root')
        devices.wan.expect_prompt()
        devices.wan.sendline('find . | cpio -H newc -o > ../root.cpio')
        devices.wan.expect_prompt()
        devices.wan.sendline('cd ..')
        devices.wan.expect_prompt()
        devices.wan.sendline('mkimage -A arm -O linux -T ramdisk -C none -d root.cpio root.uimage')
        devices.wan.expect_prompt()


        #Downloading images and booting
        devices.board.sendline()
        devices.board.reset(break_into_uboot=True)
        devices.board.sendline('setenv autoload no')
        devices.board.expect(devices.board.uprompt)
        
        for attempt in range(3):
            try:
                devices.board.sendline('dhcp')
                i = devices.board.expect(['TIMEOUT'] + devices.board.uprompt)
                assert i!=0,'dhcp timeout'
            except Exception as e:
                print(e)
                print("\nWe appeared to have failed because of DHCP timeout...")

        devices.board.sendline('setenv serverip 192.168.0.1')
        devices.board.expect(devices.board.uprompt)
        devices.board.sendline('tftpboot 0x01000000 192.168.0.1:/srv/tftp/{}/image'.format(branch))
        devices.board.expect(devices.board.uprompt, timeout=30)
        devices.board.sendline('tftpboot 0x02000000 192.168.0.1:/srv/tftp/{}/root/boot/dtb'.format(branch))
        devices.board.expect(devices.board.uprompt)
        devices.board.sendline('tftpboot 0x03000000 192.168.0.1:/srv/tftp/{}/root.uimage'.format(branch))
        devices.board.expect(devices.board.uprompt, timeout=50)
        devices.board.sendline('setenv bootargs "earlyprintk console=ttyS0,115200 rootfstype=ramfs initrd=0x03000000"')
        devices.board.expect(devices.board.uprompt)
        devices.board.sendline('bootz 0x01000000 0x03000000 0x02000000')
        devices.board.expect_exact('Router Turris successfully started.', timeout=100)
        devices.board.sendline()
        devices.board.expect_prompt()
