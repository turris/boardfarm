#!/usr/bin/env bash
# Dependencies:
#   GNU Bash 4.3 or compatible
#   GNU Coreutils 8.25 or compatible
#   OpenSSL 1.0 or compatible

set -e

DIRNAME="$(dirname "$(realpath "$0")")/ca"

pushd "$(mktemp --directory)"

openssl req -x509 -subj "/CN=Boardfarm" -newkey rsa -nodes -days 18250 -out "$DIRNAME/cert.pem" -keyout "$DIRNAME/key.pem"

touch index.txt
echo 01 >serial
cat >openssl.cnf <<EOF
[ca]
default_ca = CA_default

[CA_default]
new_certs_dir = .
certificate = $DIRNAME/cert.pem
private_key = $DIRNAME/key.pem
default_crl_days = 18250
default_md = sha256
database = ./index.txt
serial = ./serial
policy = match
EOF

openssl ca -verbose -gencrl -config openssl.cnf -out "$DIRNAME/crl"

popd
rm --recursive --force "$OLDPWD"
