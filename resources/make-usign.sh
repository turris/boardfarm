#!/usr/bin/env bash
# Dependencies:
#   GNU Bash 4.3 or compatible
#   GNU Coreutils 8.25 or compatible
#   usign 3e6648b1356e54bcee351b8f5dbfacc6ee9dab53 or compatible

set -e

DIRNAME="$(dirname "$(realpath "$0")")/usign"

usign -G -p "$DIRNAME/release.pub" -s "$DIRNAME/release.key"
