#!/usr/bin/env bash
# Dependencies:
#   GNU Bash 4.3 or compatible
#   GNU Coreutils 8.25 or compatible
#   ldns 1.6 or compatible

set -e

DIRNAME="$(dirname "$(realpath "$0")")/dns"

pushd "$(mktemp --directory)"

ZSK="$(ldns-keygen -a RSASHA1 .)"
KSK="$(ldns-keygen -k -a RSASHA1 .)"

mv "$ZSK.private" "$DIRNAME/zsk.private"
mv "$KSK.key" "$DIRNAME/ksk.key"
mv "$KSK.private" "$DIRNAME/ksk.private"

popd
rm --recursive --force "$OLDPWD"
