# Copyright (c) 2015
#
# All rights reserved.
#
# This file is distributed under the Clear BSD license.
# The full text can be found in LICENSE in the root directory.

import common
import os
import time
import openwrt_router
import pexpect
import power

class TurrisRouter(openwrt_router.OpenWrtRouter):

    model = ("turris")

    def __init__(self, *args, **kwargs):
        super(TurrisRouter, self).__init__(*args, **kwargs)
        self.wan_iface = "eth2"
        self.lan_iface = "br-lan"
        self.prompt = ['root\\@.*:.*# ']
        self.uprompt = ['=> ']

    def reset_initial(self):
        self.reset(break_into_uboot=True)
        self.sendline('\n')
        self.sendline('run norboot')
        self.expect('Welcome in rescue mode')
        self.expect('Erased')
        self.expect('Flash new images')
        self.expect('Root filesystem flashed.', timeout=300)
        self.expect('U-Boot')
        self.expect('Please press Enter to activate this console.', timeout=150)
        self.expect('procd: - init complete -')
        self.sendline()
        self.expect_prompt()

    def set_branch(self):
        '''Method to change the branch - kinda ugly, depends on environment

        Requires:
        - the board
          - containing Turris OS 3.8 or higher
        - a BRANCH environment variable set to the name of the desired branch

        '''
        branch = os.getenv('BRANCH', 'deploy')
        self.sendline('switch-branch {}'.format(branch))
        self.expect_prompt(timeout=90)

   
    def sendline(self, line=''):
        '''More robust sendline for old turris.
        Old turris sometimes misses something from serial console.
        This method sends the desired line to serial console and checks what is accepted by turris.
        If accepted string is matching sended one, it confirms the command by newline.
        Else it sends ctrl+C and give it next try (maximum 5).
        '''
        width = 80
        for attempt in range(5):
            super(TurrisRouter, self).sendline()
            self.expect_prompt()
            prompt = self.match.string.split('\r\n')[-1]
            line1_width = width - len(prompt) % width

            self.send(line)
            wrapped_lines = []
            wrapped_lines.append(line[:line1_width])
            for index in range(line1_width, len(line), width):
                wrapped_lines.append(line[index:index+width])
            for index in range(len(wrapped_lines) - 1):
                wrapped_lines[index] = wrapped_lines[index] + '\r\r\n'
            for wrapped_line in wrapped_lines:
                try:
                    self.expect_exact(wrapped_line, timeout=1)
                except pexpect.TIMEOUT:
                    self.sendcontrol('c')
                    break
            else:
                break
        else:
            raise ValueError()
        super(TurrisRouter, self).sendline()
           
    def usendline(self, line=''):
        super(TurrisRouter, self).sendline(line)
        

    def reset(self, break_into_uboot=False):
        '''Power-cycle this device.'''
        if not break_into_uboot:
            self.power.reset()
            return
        for attempt in range(3):
            try:
                self.sendline('reboot')
                self.expect('U-Boot', timeout=30)
                self.expect('Hit any key ')
                super(TurrisRouter, self).sendline('\n\n\n\n\n\n\n') # try really hard
                self.expect(self.uprompt, timeout=4)
                # Confirm we are in uboot by typing any command.
                # If we weren't in uboot, we wouldn't see the command
                # that we type.
                super(TurrisRouter, self).sendline('echo FOO')
                self.expect('echo FOO', timeout=4)
                self.expect(self.uprompt, timeout=4)
                return
            except Exception as e:
                print(e)
                print("\nWe appeared to have failed to break into U-Boot...")
