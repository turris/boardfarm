# Copyright (c) 2015
#
# All rights reserved.
#
# This file is distributed under the Clear BSD license.
# The full text can be found in LICENSE in the root directory.

import common
import os
import time
import turris
import openwrt_router


class OmniaRouter(openwrt_router.OpenWrtRouter):

    model = ("omnia")

    def __init__(self, *args, **kwargs):
        super(OmniaRouter, self).__init__(*args, **kwargs)
        self.wan_iface = "eth1"
        self.lan_iface = "br-lan"
        self.prompt = ['root\\@.*:.*# ']
        self.uprompt = ['=> ']

    def reset_initial(self):
        self.reset(break_into_uboot=True)
        self.sendline('setenv rescue 2')
        self.expect(self.uprompt)
        self.sendline('run rescueboot')
        self.expect_exact('Rolled back to snapshot factory', timeout=25)
        self.expect_exact('Router Turris successfully started.', timeout=100)
        self.sendline()
        self.expect_prompt()

    def usendline(self, line=''):
        self.sendline(line)

#    pointless
#    def mox_modules(self):
#        return ()

    def wifi_list(self):
        return [libwifi.WifiAtheros2(), libwifi.WifiAtheros5()]

    def wifi_drivers(self):
        drivers = []
        for wifi in self.wifi_list():
            drivers += wifi.drivers
        return drivers

    def reset(self, break_into_uboot=False):
        '''Power-cycle this device.'''
        if not break_into_uboot:
            self.power.reset()
            return
        for attempt in range(3):
            try:
                self.sendline()
                self.sendline('reset') #in case we are in u-boot
                self.sendline('reboot') #in case managed to boot
                self.expect('U-Boot', timeout=30)
                self.expect('Hit any key ')
                self.sendline('\n\n\n\n\n\n\n') # try really hard
                self.expect(self.uprompt, timeout=4)
                # Confirm we are in uboot by typing any command.
                # If we weren't in uboot, we wouldn't see the command
                # that we type.
                self.sendline('echo FOO')
                self.expect('echo FOO', timeout=4)
                self.expect(self.uprompt, timeout=4)
                return
            except Exception as e:
                print(e)
                print("\nWe appeared to have failed to break into U-Boot...")
